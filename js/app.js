const balanceElement = document.getElementById("bankBalance");
const getLoanElement = document.getElementById("getLoan");
const outStandingLoanElement = document.getElementById("outStandingLoan");

const payBalanceElement = document.getElementById("payBalance");
const workButtonElement = document.getElementById("workbtn");
const bankButtonElement = document.getElementById("bankbtn");
const repayLoanButtonElement = document.getElementById("repayloanbtn");

const computerElement = document.getElementById("computers");
const specsElement = document.getElementById("specs");

const chosenComputerElement = document.getElementById("chosenComputer");
const computerImage = document.getElementById("computerImg");
const titleElement = document.getElementById("title");
const descriptionElement = document.getElementById("description");
const priceElement = document.getElementById("price");
const buyComputerButtonElement = document.getElementById("buy");

const hideButton = document.getElementById("getLoan");
const showLoan = document.getElementById("loanHidden");
const noLoan = document.getElementById("noLoan");

let bankBalance = 0;
let loan = 0;
let outstandingLoan = 0;
let payBalance = 0;
let price = 0;
let currentComputer;
const work = 100;
let url = "https://noroff-komputer-store-api.herokuapp.com/";

let computers = [];

// Fetch api.
fetch(url + "computers")
  .then((response) => response.json())
  .then((data) => (computers = data))
  .then((computers) => addLaptopsToList(computers));

// Earn money to pay by working. If there is an outstanding loan, change work earnings.
const earnMoneyByWork = (e) => {
  if (outstandingLoan > 0) {
    payBalance = payBalance + work * 0.9;
    outstandingLoan = outstandingLoan - work * 0.1;
  } else {
    payBalance += work;
  }
  hideLoanButton_ShowLoan();
  showPayLoanButton();
  payBalanceElement.innerText = payBalance;
  outStandingLoanElement.innerText = outstandingLoan;
};

// Transfer money from pay to bank.
const transferToBank = (e) => {
  bankBalance += payBalance;
  payBalance = 0;
  balanceElement.innerText = bankBalance;
  payBalanceElement.innerText = payBalance;
};

// Hide loan button and show outstanding loan.
function hideLoanButton_ShowLoan() {
  if (outstandingLoan > 0) {
    hideButton.style.display = "none";
    showLoan.style.display = "block";
  } else {
    hideButton.style.display = "block";
    showLoan.style.display = "none";
  }
}

// Show button to pay loan when loan is active.
function showPayLoanButton() {
  var showButton = document.getElementById("repayloanbtn");
  if (outstandingLoan > 0) {
    showButton.style.display = "block";
  } else if (outstandingLoan <= 0) {
    showButton.style.display = "none";
  }
}

// Take a loan
const takeALoan = (e) => {
  loan = bankBalance * 2;
  let takeLoan = parseInt(prompt("Take a loan", loan));
  if (takeLoan <= loan) {
    outstandingLoan = takeLoan;
    bankBalance += outstandingLoan;
    outStandingLoanElement.innerText = outstandingLoan;
    balanceElement.innerText = bankBalance;
    noLoan.style.display = "none";
    hideLoanButton_ShowLoan();
    showPayLoanButton();
  } else {
    noLoan.innerText = "The bank doesnt confirm this big of a loan.";
    noLoan.style.display = "block";
  }
};

// Repay Loan
const repayLoan = (e) => {
  if (outstandingLoan <= payBalance) {
    payBalance = payBalance - outstandingLoan;
    outstandingLoan -= outstandingLoan;
    showPayLoanButton();
    hideLoanButton_ShowLoan();
    alert("You have paid off you debt");
  } else {
    outstandingLoan -= payBalance;
    payBalance -= payBalance;
  }
  outStandingLoanElement.innerText = outstandingLoan;
  payBalanceElement.innerText = payBalance;
};

// Set current computer. 
const setCurrentComputer = (computer) => {
  currentComputer = computer;
};

// Add computers to store
const addLaptopsToList = (computers) => {
  console.log(computers);
  computers.forEach((x) => addLaptopToList(x));
};

const addLaptopToList = (computer) => {
  currentComputer = document.createElement("option");
  currentComputer.value = computer.id;
  currentComputer.appendChild(document.createTextNode(computer.title));
  computerElement.appendChild(currentComputer);
};

// Show specs for chosen computer.
const handleComputerMenu = (e) => {
  currentComputer = computers[e.target.selectedIndex];
  let specs = currentComputer.specs;
  specsElement.innerText = "";
  specs.forEach((x) => addFeature(x));
};

const addFeature = (description) => {
  specsElement.innerText += description + " \n";
};

// Show chosen computer in new window.
const handleChosenComputer = (e) => {
  currentComputer = computers[e.target.selectedIndex];
  titleElement.innerText = currentComputer.title;
  descriptionElement.innerText = currentComputer.description;
  priceElement.innerText = currentComputer.price + " kr";
  if (currentComputer.id == 5) {
    let newUrl = (url + currentComputer.image).replace("jpg", "png");
    computerImage.src = newUrl;
  } else {
    computerImage.src = url + currentComputer.image;
  }
  chosenComputer.style.display = "block";
};

// Buy computer.
const buyComputer = (e) => {
  let title = currentComputer.title;
  let price = currentComputer.price;
  if (bankBalance >= price) {
    alert(`You just bought a brand new ${title} for ${price} kr.`);
    bankBalance -= price;
    balanceElement.innerText = bankBalance;
  } else {
    alert("Get back to work boi.");
  }
};

// EventListeners !
workButtonElement.addEventListener("click", earnMoneyByWork);
bankButtonElement.addEventListener("click", transferToBank);
getLoanElement.addEventListener("click", takeALoan);
repayLoanButtonElement.addEventListener("click", repayLoan);
computerElement.addEventListener("change", handleComputerMenu);
computerElement.addEventListener("change", handleChosenComputer);
buyComputerButtonElement.addEventListener("click", buyComputer);
